class Array2D extends Array {
	constructor(width, height) {
		super(width * height);
		this.width = width;
		this.height = height;
	}
	
	get(x, y) {
		return this[this.width * y + x];
	}
	
	set(x, y, value) {
		this[this.width * y + x] = value;
	}
}

class Matrix extends Array2D {
	constructor(width, height) {
		super(width, height);
	}
	
	times(matrix) {
		if (this.width != matrix.height) throw "Incompatible matrices";
		
		let m = new Matrix(matrix.width, this.height);
		
		for (let y = 0; y < m.height; y++)
			for (let x = 0; x < m.width; x++) {
				let total = 0;
				for (let i = 0; i < this.width; i++)
					total += this.get(i, y) * matrix.get(x, i);
				m.set(x, y, total);
			}
		
		return m;
	}
	
	static Identity(size) {
		let m = new Matrix(size, size);
		
		m.fill(0.0);
		
		for (let i = 0; i < size; i++)
			m.set(i, i, 1.0);
		
		return m;
	}
	
	static Stretch(...coords) {
		let m = new Matrix(coords.length + 1, coords.length + 1);
		
		m.fill(0.0);
		
		for (let i = 0; i < coords.length; i++)
			m.set(i, i, coords[i]);
		
		m.set(coords.length, coords.length, 1.0);
		
		return m;
	}
	
	static Translation(...coords) {
		let m = Matrix.Identity(coords.length + 1);
		
		for (let i = 0; i < coords.length; i++)
			m.set(coords.length, i, coords[i]);
		
		return m;
	}
	
	static Rotation(...coords) {
		const size = Math.sqrt(2 * coords.length + 0.25) + 0.5;
		if (!Number.isInteger(size)) throw "Invalid number of rotation angles";
		
		let t = Matrix.Identity(size);
		
		for (let a = 0; a < size - 1; a++)
			for (let b = a + 1; b < size; b++) {
				let r = Matrix.Identity(size);
				
				let theta = coords.pop();
				
				r.set(a, a,  Math.cos(theta));
				r.set(b, a, -Math.sin(theta));
				r.set(a, b,  Math.sin(theta));
				r.set(b, b,  Math.cos(theta));
				
				t = t.times(r);
			}
		
		let m = Matrix.Identity(size + 1);
		
		for (let y = 0; y < t.height; y++)
			for (let x = 0; x < t.width; x++)
				m.set(x, y, t.get(x, y));
		
		return m;
	}
}
