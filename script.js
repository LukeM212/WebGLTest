// Vertex shader
const vsSource = `
	attribute vec4 aVertexPosition;
	uniform mat4 uTransformMatrix;
	
	varying lowp vec4 vColor;
	
	void main() {
		gl_Position = aVertexPosition * uTransformMatrix;
		gl_Position.x /= gl_Position.z / 2.0 + 1.0;
		gl_Position.y /= gl_Position.z / 2.0 + 1.0;
		vColor = vec4((aVertexPosition.x + 1.0) / 2.0, (aVertexPosition.y + 1.0) / 2.0, (aVertexPosition.z + 1.0) / 2.0, 1.0);
	}
`;

// Fragment shader
const fsSource = `
	varying lowp vec4 vColor;
	
	void main() {
		gl_FragColor = vColor;
	}
`;

function createShader(gl, source, type) {
	// Compiles either a shader of type gl.VERTEX_SHADER or gl.FRAGMENT_SHADER
	var shader = gl.createShader(type);
	
	gl.shaderSource(shader, source);
	
	gl.compileShader(shader);
	
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
		throw "Could not compile WebGL program.\n\n" + gl.getShaderInfoLog(shader);
	
	return shader;
}












const canvas = document.querySelector("#glCanvas");
const gl = canvas.getContext("webgl");

if (gl == null)
	throw "Unable to initialize WebGL. Your browser or machine may not support it.";

const vertexShader = createShader(gl, vsSource, gl.VERTEX_SHADER);
const fragmentShader = createShader(gl, fsSource, gl.FRAGMENT_SHADER);

const program = gl.createProgram();

gl.attachShader(program, vertexShader);
gl.attachShader(program, fragmentShader);

gl.linkProgram(program);

if (!gl.getProgramParameter(program, gl.LINK_STATUS))
	throw "Could not compile WebGL program.\n\n" + gl.getProgramInfoLog(program);

gl.useProgram(program);

const programInfo = {
	program: program,
	attribLocations: {
		vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
	},
	uniformLocations: {
		transformMatrix: gl.getUniformLocation(program, 'uTransformMatrix'),
	},
};

const buffer = gl.createBuffer();

gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

const positions = [
	 1.0,  1.0,  1.0,
	-1.0,  1.0,  1.0,
	 1.0, -1.0,  1.0,
	-1.0, -1.0,  1.0,
	-1.0, -1.0, -1.0,
	-1.0,  1.0,  1.0,
	-1.0,  1.0, -1.0,
	 1.0,  1.0,  1.0,
	 1.0,  1.0, -1.0,
	 1.0, -1.0,  1.0,
	 1.0, -1.0, -1.0,
	-1.0, -1.0, -1.0,
	 1.0,  1.0, -1.0,
	-1.0,  1.0, -1.0,
];

gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);













gl.clearColor(0.5, 0.5, 0.5, 1.0);
gl.clearDepth(1.0);
gl.enable(gl.DEPTH_TEST);
gl.depthFunc(gl.LEQUAL);
	
{
	const numComponents = 3;
	const type = gl.FLOAT;
	const normalize = false;
	const stride = 0;
	const offset = 0;
	
	gl.vertexAttribPointer(programInfo.attribLocations.vertexPosition, numComponents, type, normalize, stride, offset);
	gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);
}

const baseMatrix =
	Matrix.Stretch(Math.min(1, canvas.height / canvas.width), Math.min(1, canvas.width / canvas.height), 1)
	.times(Matrix.Stretch(0.5, 0.5, 0.5))
	.times(Matrix.Rotation(0, 0, Math.PI / 2));

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	const transformMatrix = baseMatrix
		.times(Matrix.Rotation(Date.now() / 1000, -Math.PI / 8, 0));
	
	gl.uniformMatrix4fv(programInfo.uniformLocations.transformMatrix, false, transformMatrix);
	
	const offset = 0;
	const vertexCount = 14;
	gl.drawArrays(gl.TRIANGLE_STRIP, offset, vertexCount);
	
	window.requestAnimationFrame(draw);
}

window.requestAnimationFrame(draw);
